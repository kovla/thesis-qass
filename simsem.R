
# This script contains the very first approach to the simulation.
# In this version, the functional approach is not yet in, 
# all is done in the batch mode, step by step. 
# sim.functions.R file contains the functional version, with different
# simulation settings as arguments

#### DATA GENERATION ####

library(simsem)

# What the true model looks like
popModel <- '
y ~ 2.5*x1 + 0.5*x2 + -1.7*x3
x1 ~~ 0.8*x2
x1 ~~ 0.2*x3
x2 ~~ 0.05*x3
y ~~ 16*y
x2 ~~ 9*x2
'
# Not sure how this is used, since we are getting data only
analyzeModel <- '
y ~ x1 + x2 + x3
'

# Complete simulated data, no missings
Output <- sim(1000, analyzeModel, n=800, generate=popModel, lavaanfun = "cfa",dataOnly=TRUE,seed=1234)


# Specifying missing mechanism
missing.mechanism <- '
y ~ p(0.2) + 0.2*x1 + 0.05*x2
x1 ~ p(0.3) + 0.3*x2
'
#dat2 <- imposeMissing(dat,logit=missing.mechanism)
#plotLogitMiss(missing.mechanism)
#summary(dat2)

# Imposing missings on the data
set.seed(1234)
simdat <- lapply(Output,function(x) {imposeMissing(x,logit=missing.mechanism)})


#### DATA GENERATION AND ML-ANALYSIS IN ONE GO ####

mm <- miss(logit=missing.mechanism)
dat <- sim(1000, analyzeModel, n=800, miss = mm, generate=popModel, lavaanfun = "cfa",seed=1234)


#### REAL VALUES BASED ON THE DRAWN SAMPLES ####


#### MEANS ####

# True values (based on the average through all samples)
(true.mean.y <- mean(unlist(lapply(Output,function(x) {mean(x$y)}))))
(true.mean.x1 <- mean(unlist(lapply(Output,function(x) {mean(x$x1)}))))
(true.mean.x2 <- mean(unlist(lapply(Output,function(x) {mean(x$x2)}))))
(true.mean.x3 <- mean(unlist(lapply(Output,function(x) {mean(x$x3)}))))
true.means <- c(true.mean.y, true.mean.x1, true.mean.x2, true.mean.x3)


# Before imputation
means.y <- unlist(lapply(simdat,function(x) mean(x$y,na.rm=T)))
means.x1 <- unlist(lapply(simdat,function(x) mean(x$x1,na.rm=T)))
means.x2 <- unlist(lapply(simdat,function(x) mean(x$x2,na.rm=T)))
means.x3 <- unlist(lapply(simdat,function(x) mean(x$x3,na.rm=T)))


miss.means <- unlist(lapply(list(means.y,means.x1,means.x2,means.x3),mean,na.rm=T))


# Imputation
library(mice)

imp.list <- lapply(simdat,function(x) {mice(x,method="norm",m=10)})

getPooledMeans <- function(imp) {
    
    n.imp <- imp$m # number of imputations
    
    # matrix of means size N.imputations x N.variables
    mean.matrix <- t(sapply(1:n.imp,function(x) unlist(lapply(complete(imp,x),mean))))
    apply(mean.matrix,2,mean) # means per variable across imputations
}

mean.list <- do.call(rbind,lapply(imp.list,getPooledMeans))
(imp.means <- apply(mean.list,2,mean))
true.means
miss.means

means.dat <- cbind(true.means,imp.means,miss.means)
library(reshape2)
means.dat.m <- melt(means.dat)

library(ggplot2)
p.means <- ggplot(data=means.dat.m,aes(x=Var1,y=value,fill=Var2))
p.means + geom_bar(stat="identity",position="dodge") + xlab("Variable") +
    scale_fill_discrete(name="Method",labels=c("True means","Means with imputation","Means with NA")) + theme_bw()


#### REGRESSION COEFFICIENTS #####

# Imputations
regmods <- lapply(imp.list, function(x) {with(data=x,exp=lm(y ~ x1 + x2 + x3))})
pooled.regmods <- lapply(regmods,pool)
pooled.regsummary <- lapply(pooled.regmods,summary)

imp.reg <- do.call(rbind,lapply(pooled.regsummary, function(x) x[,1]))
imp.regmods.coef <- apply(imp.reg,2,mean)

# Semi-True values (averaged over samples, not really the true model)
true.regmods <- lapply(Output, function(x) {with(data=x,coef(lm(y ~ x1 + x2 + x3)))})
true.regmods.coef <- apply(do.call(rbind,true.regmods),2,mean)

# Regressions without imputations
miss.regmods <- lapply(simdat, function(x) {with(data=x,coef(lm(y ~ x1 + x2 + x3)))})
miss.regmods.coef <- apply(do.call(rbind,miss.regmods),2,mean)

regression.dat <- cbind(true.regmods.coef,miss.regmods.coef,imp.regmods.coef)
bias.imp <- regression.dat[,1] - regression.dat[,3]
bias.miss <- regression.dat[,1] - regression.dat[,2]
round(cbind(bias.imp,bias.miss),5)


#### REGRESSION STANDARD ERRORS ####

# Coverage
intervals <- lapply(pooled.regsummary, function(x) x[,c("lo 95","hi 95")])
isin <- lapply(intervals, function(x) {true.regmods.coef > x[,1] & true.regmods.coef < x[,2]})
isin.mat <- do.call(rbind,isin)
(coverage.reg <- colMeans(isin.mat))

